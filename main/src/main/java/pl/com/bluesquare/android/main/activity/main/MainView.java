package pl.com.bluesquare.android.main.activity.main;

import pl.com.bluesquare.android.mvp.core.view.MvpView;

/**
 * Created by Luk.
 */
public interface MainView extends MvpView {

    public void switchFragment(String fragmentName);
}
