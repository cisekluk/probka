package pl.com.bluesquare.android.main.activity.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import pl.com.bluesquare.android.main.R;

/**
 * Created by work on 04.07.15.
 */
public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ViewHolder> {


    private String[] titles;
    private int[] icons;

    public static final String name = "Lukasz";

    public final static int HEADER = 0;
    public final static int ITEM = 1;

    public DrawerAdapter(String[] titles, int[] icons) {
//        super(0, titles);
        this.titles = titles;
        this.icons = icons;
    }


    @Override
    public DrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if(viewType == HEADER) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.header_navigation_drawer, viewGroup, false);
            DrawerAdapter.ViewHolder holder = new DrawerAdapter.ViewHolder(view, viewType);
            return holder;

        } else if (viewType == ITEM){
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_navigation_drawer, viewGroup, false);
            DrawerAdapter.ViewHolder holder = new DrawerAdapter.ViewHolder(view, viewType);
            return holder;

        }
        return null;
    }




    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        if(viewHolder.holderId == ITEM) {
            viewHolder.imgIcon.setImageResource(icons[position]);
            viewHolder.txtText.setText("" + titles[position]);
        } else if (viewHolder.holderId == HEADER){
            viewHolder.txtName.setText(""+name);
        }
    }

    @Override
    public int getItemCount() {
        return titles.length;
    }

    @Override
    public int getItemViewType(int position) {
        if(isHeaderPosition(position)) {
            return HEADER;
        } else {
            return ITEM;
        }
    }

    private boolean isHeaderPosition(int position){
        return position == HEADER;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        int holderId;

        ImageView imgIcon;
        TextView txtText;

        TextView txtName;

        public ViewHolder(View itemView, int id) {
            super(itemView);

            if(id == DrawerAdapter.ITEM) {
                imgIcon = (ImageView)itemView.findViewById(R.id.item_nav_img_icon);
                txtText = (TextView)itemView.findViewById(R.id.item_nav_txt_text);
                holderId = 1;
            } else if (id == DrawerAdapter.HEADER)  {
                txtName = (TextView)itemView.findViewById(R.id.header_nav_txt_name);
                holderId = 0;
            }
        }
    }

}
