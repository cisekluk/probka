package pl.com.bluesquare.android.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Luk on 08.10.15.
 */
public class UpdateUserResponse {

    @SerializedName("updateAt")
    public String updateAt;

    @Override
    public String toString() {
        return "UpdateUserResponse{" +
                "updateAt='" + updateAt + '\'' +
                '}';
    }
}
