package pl.com.bluesquare.android.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Luk on 08.10.15.
 */
public class SigninResponse {

    @SerializedName("username")
    public String username;

    @SerializedName("phone")
    public String phone;

    @SerializedName("createdAt")
    public String createAt;

    @SerializedName("objectId")
    public String objectId;

    @SerializedName("sessionToken")
    public String token;

    @Override
    public String toString() {
        return "SigninResponse{" +
                "username='" + username + '\'' +
                ", phone='" + phone + '\'' +
                ", createAt='" + createAt + '\'' +
                ", objectId='" + objectId + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}
