package pl.com.bluesquare.android.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Luk on 08.10.15.
 */
public class SignupResponse {

    @SerializedName("createdAt")
    public String createAt;

    @SerializedName("objectId")
    public String objectId;

    @SerializedName("sessionToken")
    public String sessionToken;


    @Override
    public String toString() {
        return "SignupResponse{" +
                "createAt='" + createAt + '\'' +
                ", objectId='" + objectId + '\'' +
                ", sessionToken='" + sessionToken + '\'' +
                '}';
    }
}
