package pl.com.bluesquare.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by work on 03.07.15.
 */
public class User {

    @SerializedName("objectId")
    public String id;

    @SerializedName("username")
    public String username;

    @SerializedName("sessionToken")
    public String token;


    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}
