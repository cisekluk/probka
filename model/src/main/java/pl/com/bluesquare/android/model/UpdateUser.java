package pl.com.bluesquare.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Luk on 08.10.15.
 */
public class UpdateUser {

    @SerializedName("nick")
    public String nick;

    @SerializedName("firstName")
    public String firstName;

    @SerializedName("lastName")
    public String lastName;

    public UpdateUser(String nick, String firstName, String lastName) {
        this.nick = nick;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "UpdateUser{" +
                "nick='" + nick + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
