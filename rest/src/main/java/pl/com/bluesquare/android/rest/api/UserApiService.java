package pl.com.bluesquare.android.rest.api;

import java.util.Map;

import pl.com.bluesquare.android.model.Auth;
import pl.com.bluesquare.android.model.ResetPassword;
import pl.com.bluesquare.android.model.UpdateUser;
import pl.com.bluesquare.android.model.User;
import pl.com.bluesquare.android.model.response.ResetPasswordResponse;
import pl.com.bluesquare.android.model.response.SigninResponse;
import pl.com.bluesquare.android.model.response.SignupResponse;
import pl.com.bluesquare.android.model.response.UpdateUserResponse;
import pl.com.bluesquare.android.model.response.UserResponse;
import retrofit.Call;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.QueryMap;

/**
 * Created by Luk on 07.10.15.
 */
public interface UserApiService {



    @POST("users")
    public Call<SignupResponse> signup(@Body Auth auth);

    @GET("login")
    Call<SigninResponse> signin(@QueryMap Map<String, String> params);

    @POST("requestPasswordReset")
    Call<ResetPasswordResponse> resetPassword(@Body ResetPassword reset);

//    @POST("/logout")
//    public void logout(Callback<User> callback);


    @GET("users/{id}")
    Call<UserResponse> getUser(@Path("id") String userId);


//    @POST("/users")
//    public void postUser(@Body User user, Callback<User> callback);


    @PUT("users/{id}")
    Call<UpdateUserResponse> updateUser(@Path("id") String id, @Body UpdateUser user);

}
