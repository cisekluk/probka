package pl.com.bluesquare.android.rest.clients;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Map;

import pl.com.bluesquare.android.model.Auth;
import pl.com.bluesquare.android.model.ResetPassword;
import pl.com.bluesquare.android.model.UpdateUser;
import pl.com.bluesquare.android.model.response.ResetPasswordResponse;
import pl.com.bluesquare.android.model.response.SigninResponse;
import pl.com.bluesquare.android.model.response.SignupResponse;
import pl.com.bluesquare.android.model.response.UpdateUserResponse;
import pl.com.bluesquare.android.model.response.UserResponse;
import pl.com.bluesquare.android.rest.BaseRestClient;
import pl.com.bluesquare.android.rest.api.UserApiService;
import retrofit.Call;
import retrofit.Callback;

/**
 * Created by Luk on 07.10.15.
 */
public class UserRestClient extends BaseRestClient {

    private UserApiService apiService;

    public UserRestClient() {
        super();
    }

    public UserRestClient(String sessionToken) {
        super(sessionToken);
    }

    public void getSignup(Auth auth, Callback<SignupResponse> callback) {
        apiService = mRestAdapter.create(UserApiService.class);
        apiService.signup(auth).enqueue(callback);
    }

    public void getSignin(Map<String, String> map, Callback<SigninResponse> callback) {
        apiService = mRestAdapter.create(UserApiService.class);
        apiService.signin(map).enqueue(callback);
    }

    public void getUpdateUser(String userId, UpdateUser userUpdate, Callback<UpdateUserResponse> callback) {
        apiService = mRestAdapter.create(UserApiService.class);
        apiService.updateUser(userId, userUpdate).enqueue(callback);
    }

    public void getUser(String userId, Callback<UserResponse> callback) {
        apiService = mRestAdapter.create(UserApiService.class);
        apiService.getUser(userId).enqueue(callback);
    }

    public void getResetPassword(ResetPassword reset, Callback<ResetPasswordResponse> callback) {
        apiService = mRestAdapter.create(UserApiService.class);
        apiService.resetPassword(reset).enqueue(callback);
    }

}
