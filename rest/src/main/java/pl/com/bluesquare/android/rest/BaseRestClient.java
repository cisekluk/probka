package pl.com.bluesquare.android.rest;


import android.text.TextUtils;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by Luk.
 */
public abstract class BaseRestClient {

    protected static final String BASE_URL = "https://api.parse.com/1/";
    protected static final String APPLICATION_ID = "VTeqRc2UDuXcgiAH2cl97znIaJGj9JMHYZiEKglO";
    protected static final String APPLICATION_REST_ID = "oaUePzViMj1qiUBDegSPR5eVXZNR4VjcQy1wCxVp";
    protected Retrofit mRestAdapter;
    private String token;

    public BaseRestClient() {
        this("");
    }

    public BaseRestClient(String sessionToken) {
        this.token = sessionToken;
        init();
    }

    private void init() {
        OkHttpClient client = new OkHttpClient();
        client.networkInterceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                if(TextUtils.isEmpty(token)) {
                    return chain.proceed(getRequest(original));
                } else {
                    return chain.proceed(getRequestWithToken(original));
                }
            }
        });
        client.interceptors().add(new LoggingInterceptor());

        mRestAdapter = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    private Request getRequest(Request original) {
        Request request = original.newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("X-Parse-Application-Id", APPLICATION_ID)
                .addHeader("X-Parse-REST-API-Key", APPLICATION_REST_ID)
                .method(original.method(), original.body())
                .build();

        return request;
    }

    private Request getRequestWithToken(Request original) {
        Request request = original.newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("X-Parse-Application-Id", APPLICATION_ID)
                .addHeader("X-Parse-REST-API-Key", APPLICATION_REST_ID)
                .addHeader("X-Parse-Session-Token", token)
                .method(original.method(), original.body())
                .build();

        return request;
    }

    private Gson getCheckConverter() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
//                .registerTypeAdapter(SignupResponse.class, new SignupDeserializer())
//                .registerTypeAdapterFactory(new CheckAdapter())
                .create();
        return (gson);
    }

}