package pl.com.bluesquare.android.rest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import pl.com.bluesquare.android.model.ResetPassword;
import pl.com.bluesquare.android.model.response.ResetPasswordResponse;
import pl.com.bluesquare.android.rest.clients.UserRestClient;
import retrofit.Callback;
import retrofit.Response;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Luk on 08.10.15.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserResetTest {

    public UserRestClient restClient;
    public AtomicReference<Response<ResetPasswordResponse>> responseRef;
    public CountDownLatch latch;

    @Before
    public void setUp() {
        restClient = new UserRestClient();
        responseRef = new AtomicReference<>();
        latch = new CountDownLatch(1);
    }

    @After
    public void tearDown() {
        restClient = null;
        responseRef = null;
        latch = null;
    }

    @Test
    public void testResetSuccess() throws Exception {
        ResetPassword reset = new ResetPassword("cisek.luk@gmail.com");

        restClient.getResetPassword(reset, new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Response<ResetPasswordResponse> response) {
                System.out.println("Show code :: " + response.code());
                System.out.println("Show is success :: " + response.isSuccess());
                System.out.println("Show is msg :: " + response.message());
                if (response.code() == 200 || response.code() == 201) {
                    System.out.println("Show :: " + response.body().toString());
                }
                responseRef.set(response);
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        latch.await(5, TimeUnit.SECONDS);

        Response<ResetPasswordResponse> res = responseRef.get();
        assertThat(res).isNotNull();
        assertThat(res.isSuccess()).isTrue();
        assertThat(res.code()).isEqualTo(200);
    }

    @Test
    public void testResetFailed() throws Exception {
        ResetPassword reset = new ResetPassword("cisek.l@gmail.com");

        restClient.getResetPassword(reset, new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Response<ResetPasswordResponse> response) {
                System.out.println("Show code :: " + response.code());
                System.out.println("Show is success :: " + response.isSuccess());
                System.out.println("Show is msg :: " + response.message());
                if (response.code() == 200 || response.code() == 201) {
                    System.out.println("Show :: " + response.body().toString());
                }
                responseRef.set(response);
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        latch.await(5, TimeUnit.SECONDS);

        Response<ResetPasswordResponse> res = responseRef.get();
        assertThat(res).isNotNull();
        assertThat(res.isSuccess()).isFalse();
        assertThat(res.code()).isEqualTo(400);
    }
}
