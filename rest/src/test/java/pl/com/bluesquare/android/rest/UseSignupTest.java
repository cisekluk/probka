package pl.com.bluesquare.android.rest;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import pl.com.bluesquare.android.model.Auth;
import pl.com.bluesquare.android.model.response.SignupResponse;
import pl.com.bluesquare.android.rest.clients.UserRestClient;
import retrofit.Callback;
import retrofit.Response;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Luk on 08.10.15.
 */
@RunWith(MockitoJUnitRunner.class)
public class UseSignupTest {

    public UserRestClient restClient;
    public AtomicReference<Response<SignupResponse>> responseRef;
    public CountDownLatch latch;

    @Before
    public void setUp() {
        restClient = new UserRestClient();
        responseRef = new AtomicReference<>();
        latch = new CountDownLatch(1);
    }

    @After
    public void tearDown() {
        restClient = null;
        responseRef = null;
        latch = null;
    }

    @Test
    public void testSignupSuccess() throws InterruptedException, IOException {
        String email = "test" + new Random().nextInt() + "@tt.com";
        Auth auth = new Auth(email, email, "1234");
        System.out.println("auth :: " + auth.toString());

        restClient.getSignup(auth, new Callback<SignupResponse>() {
            @Override
            public void onResponse(Response<SignupResponse> response) {
                System.out.println("Show code :: " + response.code());
                System.out.println("Show is success :: " + response.isSuccess());
                System.out.println("Show is msg :: " + response.message());
                if (response.code() == 200 || response.code() == 201) {
                    System.out.println("Show :: " + response.body().toString());
                }
                responseRef.set(response);
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        latch.await(5, TimeUnit.SECONDS);

        Response<SignupResponse> res = responseRef.get();
        assertThat(res).isNotNull();
        assertThat(res.isSuccess()).isTrue();
        assertThat(res.code()).isEqualTo(201);
        assertThat(res.body().objectId).isNotEmpty();
        assertThat(res.body().sessionToken).isNotEmpty();
    }

    @Test
    public void testSignupFailed() throws Exception {
        Auth auth = new Auth("test@t.com", "test@t.com", "1234");
        System.out.println("auth :: " + auth.toString());

        restClient.getSignup(auth, new Callback<SignupResponse>() {
            @Override
            public void onResponse(Response<SignupResponse> response) {
                System.out.println("Show code :: " + response.code());
                System.out.println("Show is success :: " + response.isSuccess());
                System.out.println("Show is msg :: " + response.message());
                if (response.code() == 200 || response.code() == 201) {
                    System.out.println("Show :: " + response.body().toString());
                }
                responseRef.set(response);
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        latch.await(5, TimeUnit.SECONDS);

        Response<SignupResponse> res = responseRef.get();
        assertThat(res).isNotNull();
        assertThat(res.isSuccess()).isFalse();
        assertThat(res.code()).isEqualTo(400);
        assertThat(res.body()).isNull();

    }

    @Test
    public void testSignupEmptyUsernameFailed() throws Exception {
        String email = "test" + new Random().nextInt() + "@tt.com";
        Auth auth = new Auth("", email, "1234");
        System.out.println("auth :: " + auth.toString());

        restClient.getSignup(auth, new Callback<SignupResponse>() {
            @Override
            public void onResponse(Response<SignupResponse> response) {
                System.out.println("Show code :: " + response.code());
                System.out.println("Show is success :: " + response.isSuccess());
                System.out.println("Show is msg :: " + response.message());
                if (response.code() == 200 || response.code() == 201) {
                    System.out.println("Show :: " + response.body().toString());
                }
                responseRef.set(response);
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        latch.await(5, TimeUnit.SECONDS);

        Response<SignupResponse> res = responseRef.get();
        assertThat(res).isNotNull();
        assertThat(res.isSuccess()).isFalse();
        assertThat(res.code()).isEqualTo(400);
        assertThat(res.body()).isNull();

    }


    // TODO: PASSWORD COULD BE EMPTY ON REGISTER! PARSE SPECIFICATION
    @Test
    public void testSignupEmptyPasswordFailed() throws Exception {
        String email = "test" + new Random().nextInt() + "@tt.com";
        Auth auth = new Auth(email, email, "");
        System.out.println("auth :: " + auth.toString());

        restClient.getSignup(auth, new Callback<SignupResponse>() {
            @Override
            public void onResponse(Response<SignupResponse> response) {
                System.out.println("Show code :: " + response.code());
                System.out.println("Show is success :: " + response.isSuccess());
                System.out.println("Show is msg :: " + response.message());
                if (response.code() == 200 || response.code() == 201) {
                    System.out.println("Show :: " + response.body().toString());
                }
                responseRef.set(response);
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        latch.await(5, TimeUnit.SECONDS);

        Response<SignupResponse> res = responseRef.get();
        assertThat(res).isNotNull();
        assertThat(res.isSuccess()).isFalse();
        assertThat(res.code()).isEqualTo(400);
        assertThat(res.body()).isNull();

    }
}
