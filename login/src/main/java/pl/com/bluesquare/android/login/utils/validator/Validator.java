package pl.com.bluesquare.android.login.utils.validator;

/**
 * Created by Luk.
 */
public interface Validator {

    boolean isValid(String... values);

    Throwable getError();
}
