package pl.com.bluesquare.android.login.utils;

/**
 * Created by work on 05.07.15.
 */
public class FragmentName {


    public static final String FRAGMENT_KEY = "fragment_key";

    public static final String LOGIN_FRAGMENT_TAG = "login_fragment_tag";

    public static final String REGISTER_FRAGMENT_TAG = "register_fragment_tag";

    public static final String RESETPSWD_FRAGMENT_TAG = "resetpswd_fragment_tag";

    public static final String USERDETAILS_FRAGMENT_TAG = "userdetails_fragment_tag";

    public static final String FINISH = "finish";


}
