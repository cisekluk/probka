package pl.com.bluesquare.android.login.utils.exceptions;

/**
 * Created by Luk.
 */
public class PasswordNotMatchException extends Exception {

    public PasswordNotMatchException() {
        super();
    }
}
