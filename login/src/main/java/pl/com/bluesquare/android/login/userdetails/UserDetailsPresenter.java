package pl.com.bluesquare.android.login.userdetails;


import de.greenrobot.event.EventBus;
import pl.com.bluesquare.android.login.utils.FragmentName;
import pl.com.bluesquare.android.login.utils.events.ActionEvent;
import pl.com.bluesquare.android.login.utils.events.FragmentSwitchEvent;
import pl.com.bluesquare.android.login.utils.exceptions.EmailEmptyException;
import pl.com.bluesquare.android.login.utils.validator.BaseValidator;
import pl.com.bluesquare.android.login.utils.validator.UserDetailsValidator;
import pl.com.bluesquare.android.model.UpdateUser;
import pl.com.bluesquare.android.model.response.UpdateUserResponse;
import pl.com.bluesquare.android.mvp.retrofit.presenter.RetrofitPresenter;
import pl.com.bluesquare.android.rest.clients.UserRestClient;
import retrofit.Response;

/**
 * Created by Luk on 07.10.15.
 */
public class UserDetailsPresenter extends RetrofitPresenter<UserDetailsView, UpdateUserResponse> {

    private BaseValidator validator;
    private UserRestClient restClient;

    public UserDetailsPresenter() {
        super();
        restClient = new UserRestClient();
        validator = new UserDetailsValidator();
    }

    public void execute(String id, String nick, String firstName, String lastName) {
        if (isViewAttached()) {
            if (validator.isValid(nick, firstName, lastName)) {
                getView().showLoading();
                restClient.getUpdateUser(id, new UpdateUser(nick, firstName, lastName), this);

            } else {
                getView().showError(validator.getError());
            }
        }
    }

    @Override
    public void onResponse(Response<UpdateUserResponse> response) {
        if (isViewAttached() && response.isSuccess()) {
            getView().showSuccess();

        } else if (isViewAttached()) {
            getView().showError(new EmailEmptyException());
        }
    }

    public void skip() {
        if (isViewAttached()) {
            EventBus.getDefault().post(new FragmentSwitchEvent(FragmentName.FINISH));
        }
    }

    public void nointernet() {
        if (isViewAttached()) {
            EventBus.getDefault().post(new ActionEvent(ActionEvent.ACTION_SHOW_SNACKBAR_NOINTERNET));
        }
    }

}
