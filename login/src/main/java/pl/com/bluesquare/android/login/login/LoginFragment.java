package pl.com.bluesquare.android.login.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.net.UnknownHostException;

import pl.com.bluesquare.android.login.AppBaseFragment;
import pl.com.bluesquare.android.login.R;
import pl.com.bluesquare.android.login.utils.BundleNames;
import pl.com.bluesquare.android.login.utils.PreferenceName;
import pl.com.bluesquare.android.login.utils.exceptions.EmailEmptyException;
import pl.com.bluesquare.android.login.utils.exceptions.EmailFormatException;
import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;
import pl.com.bluesquare.android.login.utils.exceptions.PasswordEmptyException;
import pl.com.bluesquare.android.login.utils.exceptions.UserNotRegisterException;
import pl.com.bluesquare.android.login.utils.wrapper.SnackbarWrapper;
import pl.com.bluesquare.android.model.User;
import pl.com.bluesquare.android.model.response.SigninResponse;
import pl.com.bluesquare.android.model.response.UserResponse;
import pl.com.bluesquare.android.mvp.retrofit.exception.NetworkException;
import pl.com.bluesquare.android.utils.PreferenceHelper;

/**
 * Created by Luk on 28.06.15.
 */
public class LoginFragment extends AppBaseFragment<LoginView, LoginPresenter>
        implements LoginView, View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    TextInputLayout txtILogin;
    TextInputLayout txtIPassword;
    CheckBox chbRemember;
    Button btnRegister;
    Button btnLogin;

    private PreferenceHelper mPreferenceHelper;

    private String email;
    private String password;
    private int repeat;

    @Override
    protected LoginPresenter createPresenter() {
        return new LoginPresenter();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_login;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mPreferenceHelper = new PreferenceHelper(getActivity());

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents();
        fillForm();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        onRestore(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(BundleNames.LOGIN_BUNDLE_EMAIL, txtILogin.getEditText().getText().toString());
        outState.putString(BundleNames.LOGIN_BUNDLE_PASSWORD, txtIPassword.getEditText().getText().toString());
        outState.putInt(BundleNames.LOGIN_BUNDLE_REPEAT, repeat);
    }

    @Override
    public void showForm() {
    }

    @Override
    public void showLoading() {
        reset();
    }

    @Override
    public void showError(Throwable t) {
        if (t instanceof EmptyFieldException && t.getMessage().equals("0")) {
            txtILogin.setError(getString(R.string.string_error_empty_email));

        } else if (t instanceof EmptyFieldException && t.getMessage().equals("1")) {
            txtIPassword.setError(getString(R.string.string_error_empty_email));

        } else if (t instanceof EmailFormatException) {
            txtILogin.setError(getString(R.string.string_error_wrong_email_fromat));

        } else if (t instanceof PasswordEmptyException) {
            txtIPassword.setError(getString(R.string.string_error_empty_password));

        } else if (t instanceof UserNotRegisterException) {
            presenter.userNotRegister();

        } else if (t instanceof UnknownHostException) {
            presenter.noInternet();

        }

        repeat++;
    }

    @Override
    public void saveCreadential(SigninResponse user) {
        mPreferenceHelper.setString(PreferenceName.USER_ID_PREF, user.objectId);
        mPreferenceHelper.setString(PreferenceName.TOKEN_ID_PREF, user.token);
        mPreferenceHelper.setString(PreferenceName.USER_NAME, user.username);
    }

    @Override
    public void showSuccess() {
        Intent intent = new Intent();
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login_btn_login) {
            getLoginData();
            loginUser();

        } else if (v.getId() == R.id.login_btn_register) {
            reset();
            presenter.register();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mPreferenceHelper.setBoolean(PreferenceName.LOGIN_REMEMBER_ME, isChecked);
    }

    private void initComponents() {
        txtILogin = (TextInputLayout) getView().findViewById(R.id.login_edtxt_email);
        txtIPassword = (TextInputLayout) getView().findViewById(R.id.login_edtxt_pswd);
        chbRemember = (CheckBox) getView().findViewById(R.id.login_chb_remember);
        btnLogin = (Button) getView().findViewById(R.id.login_btn_login);
        btnRegister = (Button) getView().findViewById(R.id.login_btn_register);
        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        chbRemember.setOnCheckedChangeListener(this);
    }

    private void fillForm() {
        if (mPreferenceHelper.getBoolean(PreferenceName.LOGIN_REMEMBER_ME)) {
            txtILogin.getEditText().setText("" + mPreferenceHelper.getString(PreferenceName.USER_NAME));
            txtIPassword.getEditText().setText("" + mPreferenceHelper.getString(PreferenceName.LOGIN_PASWD));
            txtIPassword.getEditText().setText("" + mPreferenceHelper.getString(PreferenceName.LOGIN_PASWD));
            chbRemember.setChecked(true);
        }
    }

    private void getLoginData() {
        email = txtILogin.getEditText().getText().toString();
        password = txtIPassword.getEditText().getText().toString();
    }

    private void loginUser() {
        if (repeat < 3) {
            presenter.execute(email, password);
        } else {
            //show snack
            presenter.reset();
            presenter.execute(email, password);

        }
    }

    private void reset() {
        txtILogin.setErrorEnabled(false);
        txtIPassword.setErrorEnabled(false);
    }

    private void onRestore(Bundle bundle) {
        if (bundle != null) {
            txtILogin.getEditText().setText(bundle.getString(BundleNames.LOGIN_BUNDLE_EMAIL));
            txtIPassword.getEditText().setText(bundle.getString(BundleNames.LOGIN_BUNDLE_PASSWORD));
            repeat = bundle.getInt(BundleNames.LOGIN_BUNDLE_REPEAT);
        }
    }
}
