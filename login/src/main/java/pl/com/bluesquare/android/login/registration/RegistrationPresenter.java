package pl.com.bluesquare.android.login.registration;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;
import pl.com.bluesquare.android.login.utils.FragmentName;
import pl.com.bluesquare.android.login.utils.events.ActionEvent;
import pl.com.bluesquare.android.login.utils.events.FragmentSwitchEvent;
import pl.com.bluesquare.android.login.utils.exceptions.EmailEmptyException;
import pl.com.bluesquare.android.login.utils.exceptions.UserNotRegisterException;
import pl.com.bluesquare.android.login.utils.exceptions.UserRegisterException;
import pl.com.bluesquare.android.login.utils.validator.BaseValidator;
import pl.com.bluesquare.android.login.utils.validator.RegistrationValidator;
import pl.com.bluesquare.android.model.Auth;
import pl.com.bluesquare.android.model.User;
import pl.com.bluesquare.android.model.response.SignupResponse;
import pl.com.bluesquare.android.mvp.retrofit.presenter.RetrofitPresenter;
import pl.com.bluesquare.android.rest.clients.UserRestClient;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by work on 05.07.15.
 */
public class RegistrationPresenter extends RetrofitPresenter<RegistrationView, SignupResponse> {

    private BaseValidator validator;
    private UserRestClient rest;

    public RegistrationPresenter() {
        validator = new RegistrationValidator();
        rest = new UserRestClient();
    }

    public void execute(String email, String paswd, String confirmPaswd) {
        if (isViewAttached()) {
            if (validator.isValid(email, paswd, confirmPaswd)) {
                getView().showLoading();
                rest.getSignup(new Auth(email, email, paswd), this);

            } else {
                getView().showError(validator.getError());
            }
        }
    }

    @Override
    public void onResponse(Response<SignupResponse> response) {
        if (isViewAttached() && response.isSuccess()) {
            getView().saveCreadential(response.body());
            getView().showSuccess();

        } else if (isViewAttached() && response.code() == 400) {
            getView().showError(new UserRegisterException());
        }
    }

    public void userDetails() {
        if (isViewAttached()) {
            EventBus.getDefault().post(new FragmentSwitchEvent(FragmentName.USERDETAILS_FRAGMENT_TAG));
        }
    }

    public void login() {
        if (isViewAttached()) {
            EventBus.getDefault().post(new FragmentSwitchEvent(FragmentName.LOGIN_FRAGMENT_TAG));
        }
    }

    public void noInternet() {
        if (isViewAttached()) {
            EventBus.getDefault().post(new ActionEvent(ActionEvent.ACTION_SHOW_SNACKBAR_NOINTERNET));
        }
    }

    public void userRegister() {
        if (isViewAttached()) {
            EventBus.getDefault().post(new ActionEvent(ActionEvent.ACTION_SHOW_SNACKBAR_USER_REGISTER));
        }
    }


}
