package pl.com.bluesquare.android.login.utils.events;

/**
 * Created by Luk.
 */
public class FragmentSwitchEvent {

    public String fragmentName;


    public FragmentSwitchEvent(String fragmentName) {
        this.fragmentName = fragmentName;
    }
}
