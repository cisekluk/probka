package pl.com.bluesquare.android.login.utils.events;

/**
 * Created by Luk on 08.10.15.
 */
public class ActionEvent {

    public static final int ACTION_SHOW_SNACKBAR = 1;
    public static final int ACTION_SHOW_SNACKBAR_NOINTERNET = 2;
    public static final int ACTION_SHOW_SNACKBAR_USER_REGISTER = 3;
    public static final int ACTION_SHOW_SNACKBAR_USER_NOT_REGISTER = 4;


    public int actionId;


    public ActionEvent(int actionId) {
        this.actionId = actionId;
    }
}
