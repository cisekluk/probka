package pl.com.bluesquare.android.login.utils.wrapper;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import pl.com.bluesquare.android.login.R;

/**
 * Created by Luk on 08.10.15.
 */
public class SnackbarWrapper {

    private View view;
    private Snackbar snac;

    public SnackbarWrapper(View view) {
        this.view = view;
    }

    public void showLong(String text, String actionText, View.OnClickListener listener) {
        show(text, actionText, listener, Snackbar.LENGTH_LONG);
    }

    public void showIndefine(String text, String actionText, View.OnClickListener listener) {
        show(text, actionText, listener, Snackbar.LENGTH_INDEFINITE);
    }

    private void show(String text, String actionText, View.OnClickListener listener, int longLength) {
        if(snac == null) {
            snac = Snackbar.make(view, text, longLength)
                    .setAction(actionText, listener)
                    .setActionTextColor(view.getContext().getResources().getColor(R.color.hc_red2));
            View view = snac.getView();
            view.setBackgroundColor(view.getContext().getResources().getColor(R.color.hc_green_dark));

        }else {
            snac.setText(text).setAction(actionText, listener);
        }

        if (!snac.isShown()) {
            snac.show();
        }
    }

}
