package pl.com.bluesquare.android.login.utils.exceptions;

/**
 * Created by Luk on 08.10.15.
 */
public class EmptyFieldException extends Exception {

    public String fieldNo;

    public EmptyFieldException(String fieldNo) {
        super(fieldNo);
    }


}
