package pl.com.bluesquare.android.login.registration;


import pl.com.bluesquare.android.model.response.SigninResponse;
import pl.com.bluesquare.android.model.response.SignupResponse;
import pl.com.bluesquare.android.mvp.retrofit.view.MvpRetrofitView;

/**
 * Created by work on 05.07.15.
 */
public interface RegistrationView extends MvpRetrofitView {

    public void saveCreadential(SignupResponse user);
}
