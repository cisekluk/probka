package pl.com.bluesquare.android.login.validator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import pl.com.bluesquare.android.login.BuildConfig;
import pl.com.bluesquare.android.login.utils.exceptions.EmailFormatException;
import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;
import pl.com.bluesquare.android.login.utils.validator.RegistrationValidator;
import pl.com.bluesquare.android.login.utils.validator.UserDetailsValidator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by Luk on 07.10.15.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class UserDetailsValidatorTest {

    private UserDetailsValidator validator;

    @Before
    public void setUp() {
        validator = new UserDetailsValidator();
    }

    @After
    public void tearDown() {
        validator = null;
    }

    @Test
    public void testValidData() {
        String field1 = "test1";
        String field2 = "test2";
        String field3 = "test3";

        assertThat(validator.isValid(field1, field2, field3)).isTrue();
    }

    @Test
    public void testEmptyField1Exception() {
        String field1 = "";
        String field2 = "test2";
        String field3 = "test3";

        assertThat(validator.isValid(field1, field2, field3)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class);
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("0");
    }

    @Test
    public void testEmptyField2Exception() {
        String field1 = "test1";
        String field2 = "";
        String field3 = "test3";

        assertThat(validator.isValid(field1, field2, field3)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class);
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("1");
    }

    @Test
    public void testEmptyField3Exception() {
        String field1 = "test1";
        String field2 = "test2";
        String field3 = "";

        assertThat(validator.isValid(field1, field2, field3)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class);
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("2");
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testWrongArgument() {

        assertThat(validator.isValid()).isFalse();
    }

}
