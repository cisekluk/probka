package pl.com.bluesquare.android.login.presenters;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;

import pl.com.bluesquare.android.login.registration.RegistrationPresenter;
import pl.com.bluesquare.android.login.registration.RegistrationView;
import pl.com.bluesquare.android.login.restorepassword.PasswordRestorePresenter;
import pl.com.bluesquare.android.login.restorepassword.PasswordRestoreView;
import pl.com.bluesquare.android.login.utils.exceptions.EmailFormatException;
import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;
import pl.com.bluesquare.android.model.response.ResetPasswordResponse;
import pl.com.bluesquare.android.model.response.SignupResponse;
import retrofit.Callback;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Luk on 08.10.15.
 */
@RunWith(RobolectricTestRunner.class)
public class PasswordRestorePresenterTest {

    private Callback<ResetPasswordResponse> callback;
    private PasswordRestoreView passwordRestoreView;
    private PasswordRestorePresenter presenter;


    @Before
    public void setUp() {
        passwordRestoreView = Mockito.mock(PasswordRestoreView.class);
        callback = Mockito.mock(Callback.class);

        presenter = new PasswordRestorePresenter();
        presenter.attachView(passwordRestoreView);
    }

    @After
    public void tearDown() {
        presenter = null;
        passwordRestoreView = null;
        callback = null;
    }

    @Test
    public void testExecute() throws Exception {
        presenter.execute("test@t.pl");

        Mockito.verify(passwordRestoreView).showLoading();
    }

    @Test
    public void testExecuteErrorEmailFromat() throws Exception {
        ArgumentCaptor<Exception> argument = ArgumentCaptor.forClass(Exception.class);
        presenter.execute("testt.pl");

        Mockito.verify(passwordRestoreView).showError(argument.capture());
        assertThat(argument.getValue()).isExactlyInstanceOf(EmailFormatException.class);
    }

    @Test
    public void testExecuteErrorsEmailEmpty() throws Exception {
        ArgumentCaptor<Exception> argument = ArgumentCaptor.forClass(Exception.class);
        presenter.execute("");

        Mockito.verify(passwordRestoreView).showError(argument.capture());
        assertThat(argument.getValue()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("0");
    }


}
