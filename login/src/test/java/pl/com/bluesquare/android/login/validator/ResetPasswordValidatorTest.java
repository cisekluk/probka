package pl.com.bluesquare.android.login.validator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import pl.com.bluesquare.android.login.BuildConfig;
import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;
import pl.com.bluesquare.android.login.utils.validator.ResetPasswordValidator;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Luk on 08.10.15.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class ResetPasswordValidatorTest {

    private ResetPasswordValidator validator;

    @Before

    public void setUp() {
        validator = new ResetPasswordValidator();
    }

    @After
    public void tearDown() {
        validator = null;
    }

    @Test
    public void testValidData() {
        String email = "test@tt.com";

        assertThat(validator.isValid(email)).isTrue();
    }

    @Test
    public void testEmpty() {
        String email = "";

        assertThat(validator.isValid(email)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testWrongArgument() {

        assertThat(validator.isValid()).isFalse();
    }


}
