package pl.com.bluesquare.android.login.presenters;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;

import pl.com.bluesquare.android.login.registration.RegistrationPresenter;
import pl.com.bluesquare.android.login.registration.RegistrationView;
import pl.com.bluesquare.android.login.utils.exceptions.EmailFormatException;
import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;
import pl.com.bluesquare.android.login.utils.exceptions.PasswordNotMatchException;
import pl.com.bluesquare.android.model.response.SignupResponse;
import retrofit.Callback;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Luk on 08.10.15.
 */
@RunWith(RobolectricTestRunner.class)
public class RegistrationPresenterTest {

    private Callback<SignupResponse> callback;
    private RegistrationView registrationView;
    private RegistrationPresenter presenter;


    @Before
    public void setUp() {
        registrationView = Mockito.mock(RegistrationView.class);
        callback = Mockito.mock(Callback.class);

        presenter = new RegistrationPresenter();
        presenter.attachView(registrationView);
    }

    @After
    public void tearDown() {
        presenter = null;
        registrationView = null;
        callback = null;
    }

    @Test
    public void testExecute() throws Exception {
        presenter.execute("test@t.pl", "1234", "1234");

        Mockito.verify(registrationView).showLoading();
    }

    @Test
    public void testExecuteErrorEmailFromat() throws Exception {
        ArgumentCaptor<Exception> argument = ArgumentCaptor.forClass(Exception.class);
        presenter.execute("testt.pl", "1234", "1234");

        Mockito.verify(registrationView).showError(argument.capture());
        assertThat(argument.getValue()).isExactlyInstanceOf(EmailFormatException.class);
    }

    @Test
    public void testExecuteErrorsEmailEmpty() throws Exception {
        ArgumentCaptor<Exception> argument = ArgumentCaptor.forClass(Exception.class);
        presenter.execute("", "1234", "1234");

        Mockito.verify(registrationView).showError(argument.capture());
        assertThat(argument.getValue()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("0");
    }

    @Test
    public void testExecuteErrorsPasswordEmpty() throws Exception {
        ArgumentCaptor<Exception> argument = ArgumentCaptor.forClass(Exception.class);
        presenter.execute("test@t.pl", "", "1234");

        Mockito.verify(registrationView).showError(argument.capture());
        assertThat(argument.getValue()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("1");
    }

    @Test
    public void testExecuteErrorsConfirmPasswordEmpty() throws Exception {
        ArgumentCaptor<Exception> argument = ArgumentCaptor.forClass(Exception.class);
        presenter.execute("test@t.pl", "1234", "");

        Mockito.verify(registrationView).showError(argument.capture());
        assertThat(argument.getValue()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("2");
    }

    @Test
    public void testExecuteErrorsPasswordNotMatch() throws Exception {
        ArgumentCaptor<Exception> argument = ArgumentCaptor.forClass(Exception.class);
        presenter.execute("test@t.pl", "1234", "asdcv");

        Mockito.verify(registrationView).showError(argument.capture());
        assertThat(argument.getValue()).isExactlyInstanceOf(PasswordNotMatchException.class);
    }

}
