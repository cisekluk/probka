package pl.com.bluesquare.android.mvp.retrofit.view;

import pl.com.bluesquare.android.mvp.core.view.MvpView;

/**
 * Created by work on 05.07.15.
 */
public interface MvpRetrofitView extends MvpView {


    public void showForm();

    public void showLoading();

    public void showError(Throwable t);

    public void showSuccess();
}
