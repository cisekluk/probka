package pl.com.bluesquare.android.mvp.retrofit.view;

/**
 * Created by Luk.
 */
public interface MvpSTRRetrofitView extends MvpRetrofitView {

    public void showForm();

    public void showLoading();

    public void showError(Throwable t);

    public void showSuccess();
}
