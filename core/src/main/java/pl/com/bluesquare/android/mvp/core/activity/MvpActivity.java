package pl.com.bluesquare.android.mvp.core.activity;

import android.os.Bundle;

import pl.com.bluesquare.android.mvp.core.presenter.MvpPresenter;
import pl.com.bluesquare.android.mvp.core.view.MvpView;

/**
 * Created by work on 05.07.15.
 */
public abstract class MvpActivity <V extends MvpView, P extends MvpPresenter> extends BaseActivity
        implements MvpView {

    protected P presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = createPresenter();
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView(false);
    }


    protected abstract P createPresenter();
}
