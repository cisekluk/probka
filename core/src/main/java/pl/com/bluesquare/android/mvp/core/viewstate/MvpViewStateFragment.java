package pl.com.bluesquare.android.mvp.core.viewstate;

import pl.com.bluesquare.android.mvp.core.fragment.MvpFragment;
import pl.com.bluesquare.android.mvp.core.presenter.MvpPresenter;
import pl.com.bluesquare.android.mvp.core.view.MvpView;
import pl.com.bluesquare.android.mvp.core.viewstate.delegate.MvpViewStateDelegateCallback;

/**
 * Created by work on 05.07.15.
 */
public abstract class MvpViewStateFragment <V extends MvpView, P extends MvpPresenter<V>>
        extends MvpFragment<V,P> implements MvpViewStateDelegateCallback<V,P> {


    protected ViewState<V> viewState;

    private boolean restoringViewState = false;

    public abstract ViewState createViewState();

//    @Override protected FragmentMvpDelegate<V, P> getMvpDelegate() {
//        if (mvpDelegate == null) {
//            mvpDelegate = new FragmentMvpViewStateDelegateImpl<V, P>(this);
//        }
//
//        return mvpDelegate;
//    }

    @Override public ViewState getViewState() {
        return viewState;
    }

    @Override public void setViewState(ViewState<V> viewState) {
        this.viewState = viewState;
    }

    @Override public void setRestoringViewState(boolean restoringViewState) {
        this.restoringViewState = restoringViewState;
    }

    @Override public boolean isRestoringViewState() {
        return restoringViewState;
    }

    @Override public void onViewStateInstanceRestored(boolean instanceStateRetained) {
        // not needed. You could override this is subclasses if needed
    }
}
