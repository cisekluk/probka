package pl.com.bluesquare.android.mvp.core.fragment;

import android.os.Bundle;
import android.view.View;

import pl.com.bluesquare.android.mvp.core.presenter.MvpPresenter;
import pl.com.bluesquare.android.mvp.core.view.MvpView;

/**
 * Created by work on 05.07.15.
 */
public abstract class MvpFragment<V extends MvpView, P extends MvpPresenter> extends BaseFragment
        implements MvpView {

    protected P presenter;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (presenter == null) {
            presenter = createPresenter();
        }
        presenter.attachView(this);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView(getRetainInstance());
    }

    protected abstract P createPresenter();
}
