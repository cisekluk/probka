package pl.com.bluesquare.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Created by Luk.
 */
public class PreferenceHelper {

    private final String PREFERENCE_NAME = "AppPreferences";
    private Context mContext;
    private SharedPreferences preferences;

    public PreferenceHelper(Context context) {
        mContext = context;
        preferences = mContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }


    //SET

    public void setFloat(String key, float value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    public void setBoolean(String key, boolean value) {
        Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void setDouble(String key, double value) {
        Editor editor = preferences.edit();
        editor.putFloat(key, (float) value);
        editor.commit();
    }


    public void setInt(String key, int value) {
        Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public void setString(String key, String value) {
        Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void setLong(String key, long value) {
        Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }


    // GET

    public String getString(String key) {
        return preferences.getString(key, "");
    }

    public long getLong(String key) {
        return preferences.getLong(key, 0);
    }

    public float getFloat(String key) {
        return preferences.getFloat(key, 0f);
    }

    public boolean getBoolean(String key) {
        return preferences.getBoolean(key, false);
    }

    public int getInt(String key) {
        return preferences.getInt(key, 0);
    }

}
