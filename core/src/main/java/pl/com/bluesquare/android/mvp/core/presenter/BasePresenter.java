package pl.com.bluesquare.android.mvp.core.presenter;

import java.lang.ref.WeakReference;

import pl.com.bluesquare.android.mvp.core.view.MvpView;

/**
 * Created by work on 05.07.15.
 */
public class BasePresenter <V extends MvpView> implements MvpPresenter<V> {

    protected final String TAG = getClass().getSimpleName().toUpperCase();

    private WeakReference<V> viewReference;

    @Override
    public void attachView(V view) {
        viewReference = new WeakReference<V>(view);
    }


    @Override
    public void detachView(boolean retainInstance) {
        if (viewReference != null) {
            viewReference.clear();
            viewReference = null;
        }
    }


    protected V getView() {
        return viewReference == null ? null : viewReference.get();
    }


    protected boolean isViewAttached() {
        return viewReference != null && viewReference.get() != null;
    }


}
