package pl.com.bluesquare.android.mvp.core.viewgroup;

import android.content.Context;

import pl.com.bluesquare.android.mvp.core.presenter.MvpPresenter;
import pl.com.bluesquare.android.mvp.core.view.MvpView;

/**
 * Created by Luk.
 */
public abstract class MvpViewGroup<V extends MvpView, P extends MvpPresenter> extends BaseViewGroup
        implements MvpView {

    protected P presenter;

    public MvpViewGroup(Context context) {
        super(context);
    }

    @Override
    protected void onCreated() {
        super.onCreated();
        if (presenter == null) {
            presenter = createPresenter();
        }
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView(true);
    }

    protected abstract P createPresenter();


}
